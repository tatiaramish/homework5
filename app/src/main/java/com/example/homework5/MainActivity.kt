package com.example.homework5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        signInButton.setOnClickListener {
            progressBar.visibility=View.VISIBLE
            signIn()
        }
    }
    private fun signIn(){
        val email=emailEditText.text.toString()
        val password=passwordEditText.text.toString()

        if(email.isNotEmpty()&&password.isNotEmpty()){
            Toast.makeText(this,"authentication is succeed",Toast.LENGTH_SHORT).show()
            val intent= Intent(this,profileActivity::class.java)
            startActivity(intent)
        }
        else{
            Toast.makeText(this,"Please fill all fields", Toast.LENGTH_SHORT).show()
        }


    }
}